"""Module containing java source code Processor class."""
from typing import Dict, List, Any

from javalang.tree import ClassDeclaration, MethodDeclaration, CompilationUnit
from tqdm import tqdm

from lib.constants import OBJECT, PATH


# pylint: disable=too-few-public-methods
class Processor:
    """Class to process Java files."""

    def __init__(self, eval_small: bool = False):
        """Initialize.

        Args:
            eval_small: if small methods should be kept in the evaluation.

        """
        self._eval_small: bool = eval_small

    def process_java_objects(
        self,
        java_objects: List[Dict[str, CompilationUnit]]
    ) -> Dict[str, str]:
        """Process java objects to find missing documentation.

        Args:
            java_objects: javalang object to process

        Returns:
            processing results contained in a dict

        """
        missing_doc: Dict[str, str] = {}
        for java_object in tqdm(java_objects, desc="Processing Java Files"):
            for object_type in java_object[OBJECT].types:

                # skip objects that are not classes
                if not isinstance(object_type, ClassDeclaration):
                    continue

                self._evaluate_class(object_type, java_object[PATH], missing_doc)
        return missing_doc

    def _evaluate_class(
        self,
        java_class: ClassDeclaration,
        file_path: str,
        missing_doc: Dict[str, str]
    ):
        """Evaluate java class methods.

        internally update the missing_doc dictionary.

        Args:
            java_class: java class to evaluate
            file_path: path to java source file
            missing_doc: dict containing the methods missing documentation

        """
        for code_block in java_class.methods:
            # skip code blocks that are not methods
            if not isinstance(code_block, MethodDeclaration):
                continue
            if not self._eval_small and (
                not code_block.body or self._has_small_body(code_block.body)
            ):
                continue
            if not code_block.documentation:
                method_key = f"{file_path}: {code_block.name}"
                method_value = code_block.position.line
                missing_doc[method_key] = method_value

    @staticmethod
    def _has_small_body(method_body: List[Any]) -> bool:
        """Check if the method body exceeds a minimum threshold to be considered small.

        A method should normally contain at least 3 lines, one  to define the code block and two to
        define the opening and closing of the code block. As such we should check that the body is
        smaller than 3 to satisfy the 6 line requirement.

        Args:
            method_body: body of the method to check

        Returns:
            if the method has a small code body or not

        """
        return len(method_body) < 3
