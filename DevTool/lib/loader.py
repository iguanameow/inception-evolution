"""Module containing java source code Loader class."""
import os
from typing import Dict, List, AnyStr

import javalang
from javalang.tree import CompilationUnit
from tqdm import tqdm

from lib.constants import OBJECT, PATH, JAVE_EXT


# pylint: disable=too-few-public-methods
class Loader:
    """Class to load Java files to be evaluated.

    Attributes:
        _main_path: main path to project source code

    """
    def __init__(self, main_path: str):
        """Initialize.

        Args:
            main_path: main path of java project to evaluate

        """
        if not os.path.exists(main_path):
            raise FileNotFoundError('File does not exist')
        self._main_path = main_path


    def collect_java_objects(self) -> List[Dict[str, CompilationUnit]]:
        """Collect java objects in directory tree.

        Returns:
            dictionary with path to the method as a key and a javalang compilation unit as value

        """
        java_files: List[str] = [os.path.join(dp, f)
                                 for dp, dn, filenames in os.walk(self._main_path)
                                 for f in filenames if os.path.splitext(f)[1] == JAVE_EXT]
        collected_objects: List = []
        for filepath in tqdm(java_files, desc="Loading Java Files"):
            parsed_object: CompilationUnit = javalang.parse.parse(self._load_data(filepath))
            collected_objects.append(
                {
                    OBJECT: parsed_object,
                    PATH: filepath
                }
            )
        return collected_objects

    @staticmethod
    def _load_data(filepath: str) -> AnyStr:
        """Load java source code.

        Args:
            filepath: filepath to the source code to be read

        Returns:
            string form of the source code

        """
        with open(filepath, 'r', encoding="utf-8") as file:
            data = file.read()
        return data
