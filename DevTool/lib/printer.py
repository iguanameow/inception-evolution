import os
import re
import sys
from datetime import datetime

from tqdm import tqdm


class bcolors:
    """Class to introduce color in printing."""
    OKCYAN = '\033[96m'
    WARNING = '\033[93m'
    ENDC = '\033[0m'


class Printer:
    """Class to print out method names/line numbers that don't have description from Java files."""
    def __init__(self, log_path: str = None):
        """Initialize.

        Args:
            log_path: log path of evaluated java project

        """
        extension = f'_{str(datetime.now().strftime("%Y%m%dT%H%M%S"))}.txt'
        if log_path and not re.match(r"\..+$", log_path):
            log_path = log_path+extension
        self._log_path = log_path if log_path else f'logs/log{extension}'
        os.makedirs(os.path.dirname(self._log_path), exist_ok=True)

    def print_output_results(self, object_issues):
        with open(self._log_path, 'w') as f:
            if len(object_issues):
                print(f"{bcolors.WARNING}found {len(object_issues)} issues!{bcolors.ENDC}")
                f.write(f"DevTool has found {len(object_issues)} issues related to Java class files listed below.")
            else:
                print(f"{bcolors.OKCYAN}No issues with current project!{bcolors.ENDC}")
                f.write("There are no issues with current project!")
                return
            print("="*20)
            f.write(
                f"\nPlease review the listed classes by adding comments to their methods."
                f"\n\n\nPath to Java class file   :   Name of method   :   "
                f"Method line number\n{'-'*100}\n")
            for object_issue, line_issue in tqdm(object_issues.items(), desc="Printing Java File Issues"):
                print(f"{object_issue}: {line_issue}")
                f.write(f"{object_issue}: {line_issue}\n\n")
            print("="*20)
