"""Unit tests for project."""
import datetime
import unittest
from unittest.mock import patch, MagicMock, call

import javalang

from lib.constants import OBJECT, PATH
from lib.loader import Loader
from lib.processor import Processor
from lib.printer import Printer


MOCK_FILE_1 = 'mock_file_general1.java'
MOCK_FILE_2 = 'mock_file_general2.java'
LOADER_MOCK_PATH = "tests/lib/mock_file_general.java"
LOADER_MOCK_PATH_1 = "tests/lib/mock_file_general1.java"
LOADER_MOCK_PROJECT_PATH = "tests/lib/mock_valid_project_with_3_files/"

MOCK_LIMITED_LINES_WITH_ISSUES = [70]
MOCK_ALL_LINES_WITH_ISSUES = [70, 80, 86, 94]
MOCK_FULL_ISSUES_COUNT = len(MOCK_ALL_LINES_WITH_ISSUES)

MOCK_TIME = datetime.datetime.now()
MOCK_PATH_NAME = "mock_path"
MOCK_PROCESSOR_OUTPUT = {MOCK_PATH_NAME: 0}
MOCK_EXPECTED_WRITE_CALLS = 1


class TestLoader(unittest.TestCase):
    """Tests for the loader class."""
    def test_filepath(self):
        """Test Loader handle invalid path."""
        with self.assertRaises(FileNotFoundError) as error_message:
            Loader(LOADER_MOCK_PATH_1)

        self.assertEqual(str(error_message.exception), 'File does not exist')

    def test_load_data(self):
        """Test load_data returns valid text content."""
        # pylint: disable=protected-access

        mock_loader = Loader(LOADER_MOCK_PATH)
        actual_load = mock_loader._load_data(LOADER_MOCK_PATH)

        with open(LOADER_MOCK_PATH, encoding="utf-8") as file:
            expected_load = file.read()

        self.assertEqual(expected_load, actual_load)

    def test_java_files(self):
        """Test collect_java_objects returns valid output."""
        mock_loader = Loader(LOADER_MOCK_PROJECT_PATH)

        java_objects_list = mock_loader.collect_java_objects()

        actual_file_count = len(java_objects_list)
        expected_file_count = 2
        self.assertEqual(expected_file_count, actual_file_count)

        count = 1
        for java_object in java_objects_list:
            file_path = java_object[PATH]
            if count == 1:
                self.assertIn(MOCK_FILE_1, file_path)
            if count == 2:
                self.assertIn(MOCK_FILE_2, file_path)

            count = count + 1


class TestProcessor(unittest.TestCase):
    """Tests for the processor class."""

    def setUp(self) -> None:
        """Initial setup."""
        self.object_to_eval = self.get_mock_evaluation_object()

    def test_process_java_objects(self):
        """Test processing java objects produces the expected output."""
        actual_output = Processor(eval_small=True).process_java_objects(self.object_to_eval)
        self.assertEqual(list(actual_output.values()), MOCK_ALL_LINES_WITH_ISSUES)

    def test_no_classes_in_java_file(self):
        """Test output when no classes are present in the Java file."""
        mock_object_to_eval = self.object_to_eval
        mock_object_to_eval[0][OBJECT].types = []
        actual_output = Processor().process_java_objects(mock_object_to_eval)
        self.assertEqual(actual_output, {})

    @patch("lib.processor.isinstance", MagicMock(return_value=False))
    def test_no_methods_in_class(self):
        """Test output when no methods are present in class."""
        mock_object_to_eval = self.object_to_eval
        actual_output = Processor().process_java_objects(mock_object_to_eval)
        self.assertEqual(actual_output, {})

    def test_has_small_body(self):
        """Test that _eval_small flag works as expected."""
        actual_output = Processor().process_java_objects(self.object_to_eval)
        self.assertEqual(list(actual_output.values()), MOCK_LIMITED_LINES_WITH_ISSUES)

        actual_output = Processor(eval_small=True).process_java_objects(self.object_to_eval)
        self.assertEqual(list(actual_output.values()), MOCK_ALL_LINES_WITH_ISSUES)

    def get_mock_evaluation_object(self):
        """Get mock java file to evaluate on."""
        mock_path = "tests/lib/mock_file_general.java"
        with open(mock_path, encoding="utf-8") as file:
            parsed_object = javalang.parse.parse(file.read())
        return [{OBJECT: parsed_object, PATH: mock_path}]


class TestPrinter(unittest.TestCase):
    """Tests for the printer class."""

    @patch("lib.printer.datetime")
    @patch('lib.printer.os.makedirs', MagicMock())
    def test_print_initiated_with_path_correctly(self, mock_datetime: MagicMock):
        """Test print initiated with path properly when no extension is provided."""
        # pylint: disable=protected-access
        mock_datetime.now.return_value = MOCK_TIME
        expected_path = f"{MOCK_PATH_NAME}_{MOCK_TIME.strftime('%Y%m%dT%H%M%S')}.txt"
        printer = Printer(MOCK_PATH_NAME)
        actual_path = printer._log_path
        self.assertEqual(expected_path, actual_path)

    @patch("lib.printer.print")
    @patch("lib.printer.open", MagicMock())
    def test_print_called_correctly(self, mock_print: MagicMock):
        """Evaluate print calls in printer."""
        expected_calls = [
            call('\x1b[93mfound 1 issues!\x1b[0m'),
            call('===================='),
            call(f'{MOCK_PATH_NAME}: 0'),
            call('====================')
        ]
        expected_calls_count = len(expected_calls)

        printer = Printer()
        printer.print_output_results(MOCK_PROCESSOR_OUTPUT)
        self.assertEqual(mock_print.call_count, expected_calls_count)
        self.assertEqual(mock_print.mock_calls, expected_calls)

    @patch("lib.printer.print", MagicMock())
    @patch("lib.printer.open")
    def test_write_called_correctly(self, mock_write):
        """Evaluate write calls in printer."""
        printer = Printer()
        printer.print_output_results(MOCK_PROCESSOR_OUTPUT)
        self.assertEqual(mock_write.call_count, MOCK_EXPECTED_WRITE_CALLS)
