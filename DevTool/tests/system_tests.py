"""System tests for project."""
import unittest
from unittest.mock import patch, MagicMock

from javalang.parser import JavaSyntaxError

from lib.loader import Loader
from lib.printer import Printer
from lib.processor import Processor

MOCK_VALID_PROJECT_PATH = "tests/lib/mock_valid_project"
MOCK_BROKEN_CODE_PROJECT_PATH = "tests/lib/mock_broken_code_project"
MOCK_INVALID_PROJECT_PATH = "tests/lib/mock_invalid_project"
INVALID_MOCK_INPUT_PATH = "tests/lib/mock_invalid_project.ext"
MOCK_OUTPUT_PATH = "mock_output_path"
MOCK_LINES_WITH_ISSUES = [70]
MOCK_ISSUES_COUNT = len(MOCK_LINES_WITH_ISSUES)


@patch('lib.printer.os.makedirs', MagicMock())
class TestSystem(unittest.TestCase):
    """System tests for the project."""

    @patch('tests.system_tests.Printer')
    def test_valid_project(self, mock_printer: MagicMock):
        """Test valid project produced expected output."""
        expected_printer_call = {}
        loader = Loader(MOCK_VALID_PROJECT_PATH)
        processor = Processor()
        java_objects = loader.collect_java_objects()
        object_issues = processor.process_java_objects(java_objects)
        mock_printer.print_output_results(object_issues)

        mock_printer.print_output_results.assert_called_once_with(expected_printer_call)

    def test_invalid_project(self):
        """Test invalid project produced expected output."""
        loader = Loader(MOCK_INVALID_PROJECT_PATH)
        processor = Processor()
        java_objects = loader.collect_java_objects()
        object_issues = processor.process_java_objects(java_objects)
        self.assertEqual(MOCK_LINES_WITH_ISSUES, list(object_issues.values()))
        self.assertEqual(len(object_issues.keys()), MOCK_ISSUES_COUNT)
        self.assertTrue("mock_file_general.java: urlValidator" in list(object_issues)[0])

    @patch('lib.printer.os.makedirs', MagicMock())
    def test_broken_java_code_caught(self):
        """Test broken project raises exception."""
        self.assertRaises(
            JavaSyntaxError,
            main_run,
            input_path=MOCK_BROKEN_CODE_PROJECT_PATH,
            output_path=MOCK_OUTPUT_PATH
        )

    def test_invalid_path_given_caught(self):
        """Test that an invalid project path is caught."""
        self.assertRaises(
            FileNotFoundError,
            main_run,
            input_path=INVALID_MOCK_INPUT_PATH,
            output_path=MOCK_OUTPUT_PATH
        )


def main_run(input_path: str, output_path: str):
    """Main simulation."""
    loader = Loader(input_path)
    processor = Processor()
    printer = Printer(output_path)
    java_objects = loader.collect_java_objects()
    object_issues = processor.process_java_objects(java_objects)
    printer.print_output_results(object_issues)
