"""Integration tests for project."""
import unittest
from unittest.mock import patch, MagicMock

from lib.loader import Loader
from lib.printer import Printer
from lib.processor import Processor

MOCK_INVALID_PROJECT_PATH = "tests/lib/mock_invalid_project"

MOCK_LINES_WITH_ISSUES = [70]
MOCK_ISSUES_COUNT = len(MOCK_LINES_WITH_ISSUES)

MOCK_PROCCESSOR_OUTPUT = {
    'lib/mock_invalid_project/mock_file_general.java: urlValidator': MOCK_LINES_WITH_ISSUES[0]
}

MOCK_EXPECTED_PRINT_CALLS = 4
MOCK_EXPECTED_WRITE_CALLS = 1


class TestIntegration(unittest.TestCase):
    """Integration tests for the project components."""

    def test_loader_to_processor(self):
        """Test loader to processor integration."""
        loader: Loader = Loader(MOCK_INVALID_PROJECT_PATH)
        processor: Processor = Processor()
        java_objects = loader.collect_java_objects()
        actual_output = processor.process_java_objects(java_objects)
        self.assertEqual(MOCK_LINES_WITH_ISSUES, list(actual_output.values()))
        self.assertEqual(len(actual_output.keys()), MOCK_ISSUES_COUNT)
        self.assertTrue("mock_file_general.java: urlValidator" in list(actual_output)[0])

    @patch("lib.printer.print")
    @patch("lib.printer.open")
    def test_processor_to_printer(self, mock_open: MagicMock, mock_print: MagicMock):
        """Test processor to printer integration."""
        printer = Printer()
        printer.print_output_results(MOCK_PROCCESSOR_OUTPUT)
        self.assertEqual(mock_open.call_count, MOCK_EXPECTED_WRITE_CALLS)
        self.assertEqual(mock_print.call_count, MOCK_EXPECTED_PRINT_CALLS)
