import argparse

from lib.loader import Loader
from lib.processor import Processor
from lib.printer import Printer


def parse_args():
    parser = argparse.ArgumentParser(
        prog='DevOps - Documentation Checker',
        description='DevOps tool to check for missing documentation in Java codebases.'
    )
    parser.add_argument(
        '-p',
        '--path',
        type=str,
        help="Parent directory to search for Java files to evaluate",
        required=True
    )
    parser.add_argument(
        '-es',
        '--eval-small',
        help="If small methods that have less than 3 body lines of code should be evaluated",
        action='store_true'
    )
    parser.add_argument(
        '-l',
        '--log',
        type=str,
        help="Directory to store logs of evaluated Java files"
    )
    args = parser.parse_args()
    return args


def call_pipeline(path, eval_small, log):
    loader = Loader(path)
    processor = Processor(eval_small)
    printer = Printer(log)
    java_objects = loader.collect_java_objects()
    object_issues = processor.process_java_objects(java_objects)
    printer.print_output_results(object_issues)


def main():
    args = parse_args()
    call_pipeline(args.path, args.eval_small, args.log)


if __name__ == '__main__':
    main()
