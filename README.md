# G-6 Project

## Inception Evolution

Software evolution project

### Project Requirements
- [ ] INCEpTION - [github.com/NN-Minhas/inception](https://github.com/NN-Minhas/inception)
- [ ] INCEpTION Installation Guide - [inception-project.github.io/releases/34.0/docs/developer-guide](https://inception-project.github.io/releases/34.0/docs/developer-guide)
- [ ] IntelliJ IDEA - [www.jetbrains.com/idea](https://www.jetbrains.com/idea)
- [ ] PlantUML Diagram Generator plugin - [plugins.jetbrains.com/plugin/15991-plantuml-diagram-generator](https://plugins.jetbrains.com/plugin/15991-plantuml-diagram-generator)
- [ ] PlantUML Integration plugin - [plugins.jetbrains.com/plugin/7017-plantuml-integration](https://plugins.jetbrains.com/plugin/7017-plantuml-integration)

### Testing Requirements
- [ ] SonarQube - [docs.sonarqube.org](https://docs.sonarqube.org)
- [ ] SonarScanner - [docs.sonarsource.com/sonarqube/latest/analyzing-source-code/scanners/sonarscanner](https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/scanners/sonarscanner)
- [ ] SoftVis3D plugin - [softvis3d.com](https://softvis3d.com)
- [ ] Doxygen - [www.doxygen.nl](https://www.doxygen.nl)
- [ ] GraphViz - [graphviz.org](https://graphviz.org)

### DevTool Requirements
- [ ] Python version 3 - [www.python.org)](https://www.python.org)
- [ ] PyCharm - [www.jetbrains.com/pycharm](https://www.jetbrains.com/pycharm)
- [ ] Qt6 - [www.qt.io/product/development-tools](https://www.qt.io/product/development-tools)
- [ ] PyLint plugin - [plugins.jetbrains.com/plugin/11084-pylint](https://plugins.jetbrains.com/plugin/11084-pylint)
- [ ] Python 3 pip - [pypi.org/project/pip](https://pypi.org/project/pip)

```
pip install python-six javalang tqdm pylint
```

# --------------------------------------------------------------------------------------
## Milestones

### Milestone 5

#### Tables of Tasks

| Task ID | Description | Classify the complexity and extent (small, medium, large) | Justify the classification (# of files change/added/deleted; # of LOC changes/added/deleted; for non-coding tasks use: pages or lines of comments) |
| ------ | ------ | ------ | ------ |
| DT7 |  Create tests for our tool | medium | # of files changed/added/deleted  |
| DT7.1 |  System tests | large | # of files changed/added/deleted  |
| DT7.2 |  Integration tests | large | # of files changed/added/deleted  |
| DT7.3 | Unit tests | large | # of files changed/added/deleted  |
| DT8 | Create a GUI to pass paths and print log | large | # of files changed/added/deleted |
| DT9 | Refactor DevTool Proposal | large | # of files changed/added/deleted |

#### Table of Contributions

| Name | Task | Contribution in % |
| ------ | ------ | ------ |
| Oshan Siriwardena | Task DT7 | 50 |
| Oshan Siriwardena | Task DT7.3 | 33 |
| Obada Al Khayat | Task DT8 | 50 |
| Georgios Efthymiou | Task DT7 | 50 |
| Georgios Efthymiou | Task DT7.1 | 100 |
| Georgios Efthymiou | Task DT7.2 | 100 |
| Georgios Efthymiou | Task DT7.3 | 66 |
| Simeon Petrov | Task DT8 | 50 |

| Obada, Georgios, Simeon, Oshan  |  Meetings   |   100    |

### Milestone 4

#### Tables of Tasks

| Task ID | Description | Classify the complexity and extent (small, medium, large) | Justify the classification (# of files change/added/deleted; # of LOC changes/added/deleted; for non-coding tasks use: pages or lines of comments) |
| ------ | ------ | ------ | ------ |
| T3.1.1 | Known Issue: when reloading Tag Editor Panel page, the short name field don't show the stored short name. | small | # of files changed/added/deleted  |
| T3.2 |  Make short name in the default view | medium | # of files changed/added/deleted  |
| T3.3 | Investigate Refactoring & Code Quality Opportunities | medium | # of files changed/added/deleted |
| DT1 |  Create scope requirements | small | # of files changed/added/deleted  |
| DT2 |  Investigate similar tools | small | # of files changed/added/deleted  |
| DT2.1 |  Find an appropriate parser for Java code | small | # of files changed/added/deleted  |
| DT3 | Create a Java code loader | large | # of files changed/added/deleted  |
| DT4 | Create a Java code Processor | large | # of files changed/added/deleted |
| DT5 | Create an output process for the result | large | # of files changed/added/deleted |
| DT6 |  Refactoring on DevTool | large | # of files changed/added/deleted |

#### Table of Contributions

| Name | Task | Contribution in % |
| ------ | ------ | ------ |
| Oshan Siriwardena | Task 3.1.1 | 30 |
| Oshan Siriwardena | Task 3.2 | 70 |
| Oshan Siriwardena | Task 3.3 | 50 |
| Oshan Siriwardena | Task DT6 | 30 |
| Obada Al Khayat | Task 3.1.1 | 70 |
| Obada Al Khayat | Task 3.2 | 30 |
| Obada Al Khayat | Task 3.3 | 50 |
| Obada Al Khayat | Task DT6 | 30 |
| Georgios Efthymiou | Task DT2 | 100 |
| Georgios Efthymiou | Task DT2.1 | 50 |
| Georgios Efthymiou | Task DT3 | 100 |
| Georgios Efthymiou | Task DT4 | 100 |
| Georgios Efthymiou | Task DT5 | 50 |
| Georgios Efthymiou | Task DT6 | 40 |
| Simeon Petrov | Task DT.1 | 100 |
| Simeon Petrov | Task DT2.1 | 50 |
| Simeon Petrov | Task DT5 | 50 |


| Obada, Georgios, Simeon, Oshan  |  Meetings   |   100    |

### Milestone 3 

#### Tables of Tasks

| Task ID | Description | Classify the complexity and extent (small, medium, large) | Justify the classification (# of files change/added/deleted; # of LOC changes/added/deleted; for non-coding tasks use: pages or lines of comments) |
| ------ | ------ | ------ | ------ |
| T2.2 | Introduce Warning in case of a regex assertion failure | medium | # of LOC changed/added/deleted |
| T2.3 | Investigate Refactoring & Code Quality Opportunities | small | # of files changed/added/deleted  |
| T3.2 | Make short name is the default view | medium | # of LOC changed/added/deleted |
| T6 | Refactor and code quality improvements | small | # of files changed/added/deleted |
| T6.1 | Limit the possible URL scheme and require that the URLs point to a particular server also define a regex used for URL validation | medium | # of LOC changed/added/deleted |
| T16.1 | Implement Hashmap to store color hints | small | # of LOC changed/added/deleted |
| T16.2 | Investigate Refactoring & Code Quality Opportunities | small | # of files changed/added/deleted |

#### Table of Contributions

| Name | Task | Contribution in % |
| ------ | ------ | ------ |
| Oshan Siriwardena | Task 2.2 | 25 |
| Oshan Siriwardena | Task 2.3 | 100 |
| Obada Al Khayat | Task 3.2 | 90 |
| Georgios Efthymiou | Task 3.2 | 10 |
| Georgios Efthymiou | Task 6 | 100 |
| Georgios Efthymiou | Task 6.1 | 100 |
| Simeon Petrov | Task 16.1 | 100 |
| Simeon Petrov | Task 16.2 | 100 |
| Obada, Georgios, Simeon, Oshan  |  Meetings   |   100    |

### Milestone 2 

#### Tables of Tasks

| Task ID | Description | Classify the complexity and extent (small, medium, large) | Justify the classification (# of files change/added/deleted; # of LOC changes/added/deleted; for non-coding tasks use: pages or lines of comments) |
| ------ | ------ | ------ | ------ |
| T2 | Regex-based validation of string features | small | # of LOC changed/added/deleted |
| T2.1 | Regex-based validation of string features | medium | # of files change |
| T2.2 | Regex-based validation of string features | small | # of LOC changed/added/deleted |
| T3 | Short description for tags in tagsets | small | # of LOC added |
| T3.1 | Short description for tags in tagsets | medium | # of files change |
| T16 | Highlight annotation tags with different colors | small | # of LOC changed |

#### Table of Contributions

| Name | Task | Contribution in % |
| ------ | ------ | ------ |
| Obada Al Khayat | Task 3 | 100 |
| Obada Al Khayat | Task 3.1 | 25 |
| Georgios Efthymiou | Task 2 | 100 |
| Georgios Efthymiou | Task 2.1 | 100 |
| Simeon Petrov | Task 16 | 100 |
| Oshan Siriwardena | Task 2.2 | 0 |
| Obada, Georgios, Simeon  |  Meetings   |   75    |
| Oshan Siriwardena | Meetings | 0 |

### Milestone 1 

#### Tables of Tasks

| Task ID | Description | Classify the complexity and extent (small, medium, large) | Justify the classification (# of files change/added/deleted; # of LOC changes/added/deleted; for non-coding tasks use: pages or lines of comments) |
| ------ | ------ | ------ | ------ |
| T2 | Regex-based validation of string features | small | classified as a "good first issue" |
| T3 | Short description for tags in tagsets | small | classified as a "good first issue" |
| T4 | Add button to add missing labels to an ontology | small | classified as a "good first issue" |
| T6 | Restrict URLs for image features | small | classified as a "good first issue" |
| T13 | Clone a project with ALL the settings (tagsets, layers, recommenders, etc.) | large | Changes require implimentation that modifies a large part of the project |
| T16 | Highlight annotation tags with different colors | medium | Not listed as a github issue |

#### Table of Contributions

| Name | Task | Contribution in % |
| ------ | ------ | ------ |
| Obada Al Khayat | Task 3 | 100 |
| Obada Al Khayat | Task 4 | 100 |
| Georgios Efthymiou | System Architecture | 100 |
| Georgios Efthymiou | Task 2 | 100 |
| Georgios Efthymiou | Task 16 | 100 |
| Simeon Petrov | Code Fragments | 100 |
| Simeon Petrov | Task 6 | 100 |
| Simeon Petrov | Task 13 | 100 |
|  Oshan Siriwardena | System Comprehension | 100 |
|  Oshan Siriwardena | Presentation Preparation | 100 |
|  All members     |    Meetings   |   100    |
